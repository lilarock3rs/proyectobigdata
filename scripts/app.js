  'use strict';

/**
 * @ngdoc overview
 * @name myApp
 * @description
 * # myApp
 *
 * Main module of the application.
 */


var app = angular.module('myApp', ['ngAnimate','ngResource','ngRoute']);



app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/search.html',
        controller: 'searchCtrl',
        controllerAs: 'searchCtrl'
        
      });
      
  })


